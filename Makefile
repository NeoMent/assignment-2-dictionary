# Define the assembler, linker, and any flags
ASM := nasm
LD := ld
ASMFLAGS := -felf64

# Gather our sources and headers
SOURCES := $(wildcard *.asm)
HEADERS := $(wildcard *.inc)
OBJECTS := $(SOURCES:.asm=.o)

# Define the names for the default target
PROGRAM := lab

# Rule to make everything (default target)
all: $(PROGRAM)

# Rule to link the program
$(PROGRAM): $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $^

# Rule to compile the sources into objects
%.o: %.asm $(HEADERS)
	$(ASM) $(ASMFLAGS) $< -o $@

# Convenience rule for running tests
test: $(PROGRAM)
	./tests.py

# Rule to clean files
clean:
	rm -f $(OBJECTS) $(PROGRAM)

# Declare the phony targets
.PHONY: all test clean

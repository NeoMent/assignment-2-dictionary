%include 'lib.inc'
%include 'dict.inc'
%include 'words.inc'

%define STDOUT 1
%define STDERR 2
%define INPUT_LEN 0x100

section .rodata
error_text: db `key too long.`, 0
no_key_text: db `key wasn\'t found.`, 0

section .text
global _start:
_start:
	sub rsp, INPUT_LEN		;free space for input

	mov rsi, INPUT_LEN		;read input
	mov rdi, rsp
	call read_string

	test rax, rax			;if error while reading, quit
	jz .read_err
	

	mov rdi, rsp			;key
	mov rsi, first_word		;dict start
	call find_word

	test rax, rax			;if key not found, error
	jz .not_found_err

	mov rdi, rax			;get len to skip key
	push rdi
	call string_length
	pop rdi

	add rdi, rax
	inc rdi
	call print_string
	call print_newline

	xor rdi, rdi
	jmp .clean_up


	.read_err:
		mov rdi, error_text
		call print_error
		jmp .clean_up

	.not_found_err:
		mov rdi, no_key_text
		call print_error

	.clean_up:
		add rsp, INPUT_LEN
		jmp exit

%define next_elem 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
            dq next_elem
            db %1, 0

            %define next_elem %2
            
        %else
            %error "incorrect 2nd arg"
            
        %endif
        
    %else
        %error "incorrect 1st arg"
        
    %endif
    
%endmacro

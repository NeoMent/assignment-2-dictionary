%include "lib.inc"

section .text
global find_word
find_word:
	;rdi: *str, rsi: *dict
	push rsi
	mov rcx, rsi

	mov rax, [rcx]				;text if addr for next element is zero
	test rax, rax
	je .not_located				;if it is, then there are no elements and we return
	
	.next_elem:
		add rcx, 8				;move to label

		mov rsi, rcx			;compare
		push rcx
		call string_equals
		pop rcx

		test rax, rax			;if equals, return found addr
		jnz .located 

		sub rcx, 8				;return from label, calc next address, if it's the end then return not found
		mov rcx, [rcx]
		test rcx, rcx
		jz .not_located

		jmp .next_elem			;try with next element
	
	.located:
		mov rax, rsi
		jmp .end
	.not_located:
		xor rax, rax
	.end:
		pop rsi
		ret

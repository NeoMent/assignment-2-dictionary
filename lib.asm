; syscalls
%define EXITSYSCODE 60
%define WRITESYSCODE 1
%define READSYSCODE 0
; IO channels and chars
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define MAXUINTLEN 20

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global read_string

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXITSYSCODE	;specify exit syscode
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax				;set strlen to zero
    .next_char:
    	cmp byte [rdi + rax], 0 ;compare with terminator
    	je .end					;if terminator, exit
    	inc rax					;else rax++ and repeat
    	jmp .next_char
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi					;store rdi
    call string_length			;get strlen


    mov rdx, rax 				;set str len
    mov rax, WRITESYSCODE		;set write syscode
    pop rsi						;set str addr
    mov rdi, STDOUT				;set stdout
    syscall

    ret

print_error:
	push rdi
	call string_length

	mov rdx, rax
	mov rax, WRITESYSCODE
	pop rsi
	mov rdi, STDERR
	syscall

	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`				;set newline char

; Принимает код символа и выводит его в stdout
print_char:
    push rdi					;push char to stack

	mov rsi, rsp				;set char stack address
    mov rdx, 1					;set length=1
    mov rax, WRITESYSCODE		;set write syscode
    mov rdi, STDOUT				;set stdout
    syscall

    add rsp, 8					;pop char from stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	mov rax, rdi
	test rax, rax
	jns print_uint				;if positive then print it
	mov rdi, '-'
	push rax
	call print_char				;else print minus
	pop rdi
	neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi				;rax = number
	mov rcx, MAXUINTLEN			;char pointer
	sub rsp, 24					;make space for string
	mov byte [rsp + MAXUINTLEN], 0 	;set null terminator
	mov r10, 10					;for division

	.next_char:
		xor rdx, rdx
		div r10					;divide by 10
		add rdx, '0'			;convert to ascii symbol
		dec rcx					;move char pointer
		mov [rsp + rcx], dl

		test rax, rax
		jne .next_char

	lea rdi, [rsp+rcx]
	call print_string
	add rsp, 24

    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rdx, rdx				;char counter
    .cmp_char:
    	mov r10b, byte [rsi+rdx];store s2 char
    	cmp r10b, byte [rdi+rdx];compaare with s1 char
    	jne .not_eq
    	test r10b, r10b			;if strings have ended, return true
    	je .eq
    	inc rdx					;inc char counter
    	jmp .cmp_char			;repeat
    .not_eq:					;return 0
    	xor rax, rax
    	ret
    .eq:						;return 1
    	mov rax, 1
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0 						;default value
    mov rdi, STDIN				;set STDIN
    mov rax, READSYSCODE		;set syscode
    mov rsi, rsp				;set write addr to top of stack
    mov rdx, 1 					;set len
    syscall

    cmp rax, -1					;if IO error, set to zero
    je .set_zero
	pop rax						;move char to ac
    ret

    .set_zero:
    	xor rax, rax
    	ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	xor r12, r12				;r12 = current length
	mov r13, rdi				;r13 = buffer addr
	mov r14, rsi				;r14 = buffer length
	.bskip:
		call read_char			;get input
		cmp rax, ' '			;skip space
		je .bskip
		cmp rax, `\n`			;skip \n
		je .bskip
		cmp rax, `\t`			;skip tab
		je .bskip
		jmp .rs_after_read
	.read_symbol:
		call read_char			;get input
	.rs_after_read:
		cmp r12, r14			;make sure we have space
		jge .its_so_over
		test rax, rax     		;if end of input, return
		je .we_are_so_back
		cmp rax, ' '
		je .we_are_so_back
		cmp rax, `\n`
		je .we_are_so_back
		cmp rax, `\t`
		je .we_are_so_back
		mov [r13 + r12], al		;save input
		inc r12					;inc current length
		jmp .read_symbol
	.we_are_so_back:
		mov byte [r13 + r12], 0
		mov rax, r13			;rax = buffer addr
		mov rdx, r12			;rdx = used length
		jmp .cleanup
	.its_so_over:
		xor rax, rax
		xor rdx, rdx
	.cleanup:
		pop r14
		pop r13
		pop r12
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax				;number
	mov r11, 10					;multiplier
	xor r9, r9					;number length
    .next_char:
    	mov r10b, [rdi + r9]	;load char
    	cmp r10b, '0'			;validate value
    	jl .end
    	cmp r10b, '9'
    	jg .end

		sub r10, '0'			;convert char to int
		inc r9					;length++
		mul r11					;rax = rax * 10 + r10
		add rax, r10
		jmp .next_char
    .end:
    	mov rdx, r9
    	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	mov r10b, [rdi]
	cmp r10b, '-'				;check signs
	je .minus
	cmp r10b, '+'
	je .plus

	cmp r10b, '9'				;check that it's a number
	jg .err
	cmp r10b, '0'
	jl .err

	jmp .no_sign
    .minus:
    	inc rdi
    	call parse_uint
    	test rdx, rdx			;if couldn;t parse uint, error
    	je .err
    	inc rdx
    	neg rax
    	ret

    .plus:
    	inc rdi
    	call parse_uint
    	test rdx, rdx			;if couldn't parse uint, error
    	je .err
    	inc rdx
    	ret

    .no_sign:
    	call parse_uint
    	ret

    .err:
    	xor rdx, rdx
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r9, r9	;copy length
    ; rdi = str addr
    ; rsi = buffer addr
    ; rdx = buffer len
    .next_char:
    	cmp rdx, r9				;if no more space in buffer, error
    	jle .no_space
    	mov al, [rdi + r9]		;read char
    	mov [rsi + r9], al		;write char
    	inc r9
    	test al, al
    	je .end
    	jmp .next_char

    .no_space:
    	xor rax, rax
    	ret

    .end:
    	mov rax, r9
    	ret

read_string:
 	push rdi      
  	push r12      
  	mov r12, rdi

  	push r13     
  	mov r13, rsi

  	;r12 - current addr
  	;r13 - used space
  	test r13, rsi
  	jz .its_over

 	.next_char:
 		call read_char			;read
    	cmp rax, 0     			;if terminator, finish
    	je .we_are_so_back

    	dec r13               	;if no more space, exit
    	cmp r13, 0
    	jbe .its_over  

    	mov byte [r12], al    	;Save char to buffer
    	inc r12               	;current addr++

    	jmp .next_char

  	.we_are_so_back:
   		mov byte [r12], 0  
    	pop r13
    	pop r12

    	mov rdi, [rsp]        
    	call string_length    
    	mov rdx, rax          

    	pop rax
    	ret

  	.its_over:
    	pop r13
    	pop r12
    	pop rdi
    	xor rax, rax          
   	 	ret

#!/usr/bin/env python3
import subprocess
import unittest

class Test(unittest.TestCase):
    def run_lab2(self, _input):
        pipe = subprocess.PIPE
        process = subprocess.Popen(['./lab'], text=True, stdin=pipe, stdout=pipe, stderr=pipe, shell=True)
        stdout, stderr = process.communicate(input=_input)
        return (stdout.strip(), stderr.strip())

    def test_length(self):
        result = ("", "key too long.")
        self.assertEqual(self.run_lab2("amogus"*500), result)

    def test_not_located(self):
        result = ("", "key wasn't found.")
        self.assertEqual(self.run_lab2("bebrasniffer"), result)

    def test_keys(self):
        result = ("first word explanation", "")
        self.assertEqual(self.run_lab2("first word"), result)

        result = ("second word explanation", "")
        self.assertEqual(self.run_lab2("second word"), result)

        result = ("third word explanation", "")
        self.assertEqual(self.run_lab2("third word"), result)

if __name__ == "__main__":
  unittest.main()
